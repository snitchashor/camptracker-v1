#!/usr/bin/python
from __future__ import print_function
from __future__ import absolute_import
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import object

import time
from datetime import datetime
from yattag import Doc,indent
import tempfile
import pickle
import sqlite3
import zlib
import os
import sys
from os.path import join, exists
import urllib2
import json
import cPickle as pickle

from contextlib import contextmanager
from collections import defaultdict
import eveapi

debug = False
for s in sys.argv:
    if 'debug' in str(s):
        debug = True

key='apikey'
code='apivcode'
name='Character Name'
keywords=['camptracker', 'Camptracker', 'camp tracker', 'Camp Tracker']

dirname=os.path.dirname(os.path.realpath(sys.argv[0]))
htmlfile=join(dirname, 'donations.html')
pfile=join(dirname, 'donations.p')

eveapi.set_user_agent("eveapi.py/1.3")

@contextmanager
def ignored(exceptions=Exception):
  try:
    yield
  except exceptions, e:
    print("Got error! ", repr(e))

class MyCacheHandler(object):
        def __init__(self, debug=False):
                self.debug = debug
                self.count = 0
                self.cache = {}
                self.tempdir = join(tempfile.gettempdir(), "eveapi")
                if not exists(self.tempdir):
                        os.makedirs(self.tempdir)

        def log(self, what):
                if self.debug:
                        print("[%d] %s" % (self.count, what))

        def retrieve(self, host, path, params):
                # eveapi asks if we have this request cached
                key = hash((host, path, frozenset(list(params.items()))))

                self.count += 1  # for logging

                # see if we have the requested page cached...
                cached = self.cache.get(key, None)
                if cached:
                        cacheFile = None
                        #print "'%s': retrieving from memory" % path
                else:
                        # it wasn't cached in memory, but it might be on disk.
                        cacheFile = join(self.tempdir, str(key) + ".cache")
                        if exists(cacheFile):
                                self.log("%s: retrieving from disk" % path)
                                f = open(cacheFile, "rb")
                                cached = self.cache[key] = pickle.loads(zlib.decompress(f.read()))
                                f.close()

                if cached:
                        # check if the cached doc is fresh enough
                        if time.time() < cached[0]:
                                self.log("%s: returning cached document" % path)
                                return cached[1]  # return the cached XML doc

                        # it's stale. purge it.
                        self.log("%s: cache expired, purging!" % path)
                        del self.cache[key]
                        if cacheFile:
                                os.remove(cacheFile)

                self.log("%s: not cached, fetching from server..." % path)
                # we didn't get a cache hit so return None to indicate that the data
                # should be requested from the server.
                time.sleep(0.04)
                return None

        def store(self, host, path, params, doc, obj):
                # eveapi is asking us to cache an item
                key = hash((host, path, frozenset(list(params.items()))))

                cachedFor = obj.cachedUntil - obj.currentTime
                if cachedFor:
                        self.log("%s: cached (%d seconds)" % (path, cachedFor))

                        cachedUntil = time.time() + cachedFor

                        # store in memory
                        cached = self.cache[key] = (cachedUntil, doc)

                        # store in cache folder
                        cacheFile = join(self.tempdir, str(key) + ".cache")
                        f = open(cacheFile, "wb")
                        f.write(zlib.compress(pickle.dumps(cached, -1)))
                        f.close()

api = eveapi.EVEAPIConnection()
cApi = eveapi.EVEAPIConnection(cacheHandler=MyCacheHandler(debug=debug))

def getDonations(key, code, name, keywords):
    auth = cApi.auth(keyID=key, vCode=code)
    try:
        # Try calling account/Characters without authentication context
        result = auth.account.Characters()
    except eveapi.Error as e:
        print(name+": Oops! eveapi returned the following error:")
        print(name+": code:", e.code)
        print(name+": message:", e.message)
        acc['error'] += 'APIkey '+str(key)+' : '+str(e.message)+' '
        acc['flag'] = 'red'
        #raise
    except Exception as e:
        print(name+": Something went horribly wrong:", str(e))
        acc['error'] += 'APIkey '+str(key)+' : '+str(e)+' '
        acc['flag'] = 'red'
        #raise
    else:
        for character in result.characters:
            if (character['name']) == name:
                c = auth.character(character.characterID)
                journal = c.WalletJournal()
        entriesByRefType = journal.transactions.GroupedBy("refTypeID")
        ids = []
        if os.path.isfile(pfile):
            donations = pickle.load(open(pfile, 'rb'))
            for donation in donations:
                ids.append(donation['id'])
        else:
            donations = []
        for refID, date, ownerName1, ownerName2, amount, reason in entriesByRefType[10].Select("refID", "date", "ownerName1", "ownerName2", "amount", "reason"):
            if (ownerName2 == name) and not refID in ids:
                for keyword in keywords:
                    if keyword in reason:
                        donation = {}
                        donation['id'] = refID
                        donation['date'] = time.strftime("%Y-%m-%d %H:%M", time.gmtime(date))
                        donation['donator'] = ownerName1
                        if amount > 1000000000:
                            donation['amount'] = ('% .1fB ISK' % (amount/1000000000))
                        elif amount > 1000000:
                            donation['amount'] = ('% .1fM ISK' % (amount/1000000))
                        elif amount > 1000:
                            donation['amount'] = ('% .1fK ISK' % (amount/1000))
                        else:
                            donation['amount'] = (str(amount)+' ISK')
                        donation['reason'] = reason.replace('DESC: ', '')
                        donations.append(donation)
        pickle.dump(donations, open(pfile, 'wb'))
        return(donations)

def createDonationsHTML(donations, output):
    doc, tag, text = Doc().tagtext()

    doc.asis('<!DOCTYPE html>')
    with tag('html'):
        with tag('body'):
            with tag('div', klass='title'):
                with tag('h2'):
                    text('Current Supporters')
                with tag('div', klass='supporters'):
                    if len(donations) == 0:
                        text('None... you greedy shipspinners!')
                    else:
                        doc.asis('<script src="sorttable.js"></script>')
                        with tag('table', klass='sortable'):
                            with tag('tr'):
                                with tag('th', klass='date'):
                                    text('Date')
                                with tag('th', klass='donator'):
                                    text('Donator')
                                with tag('th', klass='amount'):
                                    text('Amount')
                                with tag('th', klass='reason'):
                                    text('Reason')
                            for donation in donations:
                                with tag('tr'):
                                    with tag('td', klass='date'):
                                        text(donation['date'])
                                    with tag('td', klass='donator'):
                                        text(donation['donator'])
                                    with tag('td', klass='amount'):
                                        text(donation['amount'])
                                    with tag('td', klass='reason'):
                                        text(donation['reason'])
    result = (indent(doc.getvalue()))
    out = file(output, 'w')
    out.write(result)
    out.close()

donations = getDonations(key, code, name, keywords)
createDonationsHTML(donations, htmlfile)
