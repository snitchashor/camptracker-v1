<?php

$debug=false;

require_once('vendor/autoload.php');
session_start();
//session_destroy();
$provider = new Evelabs\OAuth2\Client\Provider\EveOnline([
    'clientId'          => '',
    'clientSecret'      => '',
    'redirectUri'       => 'https://yoursite.com/?p=map2',
]);

$options = [
    'scope' => ['publicData','characterLocationRead'] // array or string
];

$salt ='randomkeyhere';

function simple_encrypt($text,$salt)
{  
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
}

function simple_decrypt($text,$salt)
{  
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}

if (isset($_SESSION['access_token']) && isset($_SESSION['refresh_token'])) {
    //$token = $_SESSION['token'];
    $token = (unserialize(simple_decrypt($_SESSION['access_token'],$salt)));
    if ($token->hasExpired()) {
        $token = $provider->getAccessToken('refresh_token', ['refresh_token' => (simple_decrypt($_SESSION['refresh_token'],$salt))]);
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    }
    //print_r(unserialize(simple_decrypt(simple_encrypt(serialize($token),$salt),$salt))->getToken());
    $authUrl = $provider->getAuthorizationUrl($options);
    $_SESSION['oauth2state'] = $provider->getState();
} elseif (!isset($_GET['code'])) {
    if (isset($_SESSION['refresh_token'])) {
        $token = $provider->getAccessToken('refresh_token', ['refresh_token' => (simple_decrypt($_SESSION['refresh_token'],$salt))]);
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['oauth2state'] = $provider->getState();
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    } else { 
    // If we don't have an authorization code then get one
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['oauth2state'] = $provider->getState();
        echo "<div class='mapinfo' style='display: table;'><span style='display: table-cell; vertical-align: middle; padding-right: 10px;'>Want to track your location?</span> <a href='".$authUrl."'><img src='logineve.png'></a></div>";
    }
// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
    unset($_SESSION['oauth2state']);
    echo "<div class='mapinfo'>CREST auth: Invalid state</div>";

} else {
    if (isset($_SESSION['refresh_token'])) {
        $token = $provider->getAccessToken('refresh_token', ['refresh_token' => (simple_decrypt($_SESSION['refresh_token'],$salt))]);
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    } else {
        // Try to get an access token (using the authorization code grant)
        $token = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);
        $_SESSION['refresh_token']=simple_encrypt($token->getRefreshToken(),$salt);
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    }

    // Optional: Now you have a token you can look up a users profile data
    try {

        // We got an access token, let's now get the user's details
        $user = $provider->getResourceOwner($token);

        // Use these details to create a new profile
        //printf('Hello %s!', $user->getCharacterName());

    } catch (Exception $e) {

        // Failed to get user details
        echo "<div class='mapinfo'>Something went horribly wrong getting the location via CREST...</div>";
    }
    // Use this to interact with an API on the users behalf
    //echo $token->getToken();
}

if (isset($_SESSION['access_token'])) {
    $user = $provider->getResourceOwner($token);
    //printf('Hello %s!', $user->getCharacterName());
    $request = $provider->getAuthenticatedRequest(
        'GET',
        'https://crest-tq.eveonline.com/characters/'.$user->getCharacterID().'/location/',
        $token->getToken()
    );

    $response = $provider->getResponse($request);
    if (empty($response)) {
        if ($debug) {
            $response = [ 'solarSystem' => [ 'id_str' => '30045329', 'href' => 'https://crest-tq.eveonline.com/solarsystems/30045329/', 'id' => 30045329, 'name' => 'Ichoriya' ] ];
        }else{
            echo "<div class='mapinfo'>Your character needs to be logged in to track your location.</div>";
        }
    } else {
        $systemName = $response['solarSystem']['name'];
        $systemID = $response['solarSystem']['id'];
    }
}
