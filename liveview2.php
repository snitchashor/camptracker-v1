<?php

$debug=false;
$regions=array();

$servername = "localhost";
$username = "";
$password = "";
$dbname = "";

echo '<!DOCTYPE HTML>
<html lan="en">
<head>
<title>Camptracker - Liveview</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="stylesheet" href="liveview.css" type="text/css" />
<script>
setTimeout(function(){
   window.location.reload(1);
}, 5000);
</script>
</head>
<body>';

require_once('vendor/autoload.php');
session_start();
//session_destroy();
$provider = new Evelabs\OAuth2\Client\Provider\EveOnline([
    'clientId'          => '',
    'clientSecret'      => '',
    'redirectUri'       => 'https://yoursite.com/?p=liveview2',
]);

$options = [
    'scope' => ['publicData','characterLocationRead'] // array or string
];

$salt ='randomstringhere';

function simple_encrypt($text,$salt)
{  
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
}

function simple_decrypt($text,$salt)
{  
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}

if (isset($_SESSION['access_token']) && isset($_SESSION['refresh_token'])) {
    //$token = $_SESSION['token'];
    $token = (unserialize(simple_decrypt($_SESSION['access_token'],$salt)));
    if ($token->hasExpired()) {
        $token = $provider->getAccessToken('refresh_token', ['refresh_token' => (simple_decrypt($_SESSION['refresh_token'],$salt))]);
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    }
    //print_r(unserialize(simple_decrypt(simple_encrypt(serialize($token),$salt),$salt))->getToken());
    $authUrl = $provider->getAuthorizationUrl($options);
    $_SESSION['oauth2state'] = $provider->getState();
} elseif (!isset($_GET['code'])) {
    if (isset($_SESSION['refresh_token'])) {
        $token = $provider->getAccessToken('refresh_token', ['refresh_token' => (simple_decrypt($_SESSION['refresh_token'],$salt))]);
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['oauth2state'] = $provider->getState();
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    } else { 
    // If we don't have an authorization code then get one
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['oauth2state'] = $provider->getState();
        header('Location: '.$authUrl);
        exit;
    }
// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {
    if (isset($_SESSION['refresh_token'])) {
        $token = $provider->getAccessToken('refresh_token', ['refresh_token' => (simple_decrypt($_SESSION['refresh_token'],$salt))]);
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    } else {
        // Try to get an access token (using the authorization code grant)
        $token = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);
        $_SESSION['refresh_token']=simple_encrypt($token->getRefreshToken(),$salt);
        $_SESSION['access_token'] = (simple_encrypt(serialize($token),$salt));
    }

    // Optional: Now you have a token you can look up a users profile data
    try {

        // We got an access token, let's now get the user's details
        $user = $provider->getResourceOwner($token);

        // Use these details to create a new profile
        //printf('Hello %s!', $user->getCharacterName());

    } catch (Exception $e) {

        // Failed to get user details
        exit('Something went horribly wrong...');
    }
    // Use this to interact with an API on the users behalf
    //echo $token->getToken();
}
$user = $provider->getResourceOwner($token);
//printf('Hello %s!', $user->getCharacterName());
$request = $provider->getAuthenticatedRequest(
    'GET',
    'https://crest-tq.eveonline.com/characters/'.$user->getCharacterID().'/location/',
    $token->getToken()
);

$response = $provider->getResponse($request);
if (empty($response)) {
    if ($debug) {
        $response = [ 'solarSystem' => [ 'id_str' => '30003830', 'href' => 'https://crest-tq.eveonline.com/solarsystems/30045329/', 'id' => 30003830, 'name' => 'Ichoriya' ] ];
    }else{
        exit("Your character needs to be logged in to use this feature.</body>");
    }
}

$systemName = $response['solarSystem']['name'];
$systemID = $response['solarSystem']['id'];

$doc = new DOMDocument();
$doc->loadHTMLFile("camps.html");

function getElementsByClass(&$parentNode, $tagName, $className) {
    $nodes=array();

    $childNodeList = $parentNode->getElementsByTagName($tagName);
    for ($i = 0; $i < $childNodeList->length; $i++) {
        $temp = $childNodeList->item($i);
        if (stripos($temp->getAttribute('class'), $className) !== false) {
            $nodes[]=$temp;
        }
    }

    return $nodes;
}

$jumps = [];
$jump_node=$doc->getElementById("jumps");
$jump_divs=getElementsByClass($jump_node, 'div', 'jump');
foreach ($jump_divs as &$div) {
    $jumps[$div->getAttribute('id')] = $div->nodeValue;
}

$browser = $_SERVER['HTTP_USER_AGENT'];
if (!function_exists('getallheaders')) {
        function getallheaders() {
            foreach($_SERVER as $key=>$value) {
                if (substr($key,0,14)=="REDIRECT_HTTP_") {
                    $key=str_replace("REDIRECT_HTTP_", "", $key);
                    $out[$key]=$value;
                }else if (substr($key,0,5)=="HTTP_") {
                    $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5)))));
                    $out[$key]=$value;
                }else{
                    $out[$key]=$value;
        }
            }
            return $out;
        }
} 

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT jump.toSolarSystemID as destID, dest.solarSystemName as destName, dest.security as sec, dest.regionID as region, origin.solarSystemID as originID, origin.solarSystemName as originName, origin.security as osec FROM mapSolarSystems dest LEFT JOIN mapSolarSystemJumps jump ON dest.solarSystemID = jump.toSolarSystemID LEFT JOIN mapSolarSystems origin ON origin.solarSystemID = jump.fromSolarSystemID WHERE origin.solarSystemID = ".(string)(int)$systemID;
$result = $conn->query($sql);
echo "<div><table>";
if ($result->num_rows > 0) {
    // output data of each row
    $first = true;
    while($row = $result->fetch_assoc()) {
        if ($first) {
            $osec = round($row["osec"], 1);
            if ($osec < 0) {
                $osec = 0.0;
            }
            echo "Current system: ".$systemName." <small>(<span class=s".str_replace(".", "", $osec).">".$osec."</span>)";
            $first = false;
        }
        if ((int)$row["originID"] > (int)$row["destID"]) {
            $jump = "j-".$row["originID"]."-".$row["destID"];
        } else {
            $jump = "j-".$row["destID"]."-".$row["originID"];
        }
        if (array_key_exists($jump, $jumps)) {
            echo "<tr><td class='adjacent-camp' title='".$jumps[$jump]."'>Danger</td>";
        }else{
            if (in_array($row["region"], $regions)) {
                echo "<tr><td class='adjacent-ok'>Save</td>";
            }else{
                echo "<tr><td class='adjacent-undefined'>Undefined</td>";
            }
        }
        echo "<td class='adjacent' id='".$jump."'>".$row["destName"]." ";
        $sec = round($row["sec"], 1);
        if ($sec < 0) {
            $sec = 0.0;
        }
        echo "<small>(<span class=s".str_replace(".", "", $sec).">".$sec."</span>)</small></td></tr>";
    }
} else {
    echo "0 results";
}
echo "</table></div></body></html>";
$conn->close();
?>
