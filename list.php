<?php
$title = "Snitch's Gatecamp Tracker";
header('Expires: Tue, 01 Jan 1980 1:00:00 GMT');
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

require_once('header.php');
#require('update.php');
include('camps.html');
include('listscripts.php');
require_once('footer.php');
?>
