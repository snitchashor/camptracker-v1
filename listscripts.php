<script src="jquery-1.11.3.min.js"></script>
<script>
var slideHeight = 160;
$(".system").each(function() {
    var $wrap = $(this);
    var defHeight = $wrap.height();
    if (defHeight >= slideHeight + 40 ) {
        $wrap.append( "<div class='gradient'></div>" );
        $wrap.append( "<div class='read-more'></div>" );
        var $readMore = $wrap.find(".read-more");
        $wrap.css("height", slideHeight + "px");
	$wrap.css("overflow", "hidden");
        $victim = $wrap.find("td.victim");
        //$victim.css({"vertical-align" : "top", "padding-top" : "0"});
        $time = $wrap.find("td.time");
        //$time.css({"vertical-align" : "top", "padding-top" : "0"});
        $readMore.append("<a href='#'>Show all</a>");
        $readMore.children("a").bind("click", function(event) {
            var curHeight = $wrap.height();
            if (curHeight == slideHeight) {
                $wrap.animate({
                    height: defHeight
                }, "normal");
                //$victim.animate({
                //    'padding-top': ($(this).parent().height()/2)-36,
                //}, "normal");
                //$time.animate({
                //    'padding-top': ($(this).parent().heigth()/2)-36,
                //}, "normal");
                $(this).text("Collapse");
                $wrap.children(".gradient").fadeOut();
		$wrap.find("img").css("-webkit-border-radius", "2px");
                setTimeout(function()
                {
                  $wrap.find("img").css("-webkit-border-radius", "3px");
                }, 500);
            } else {
                $wrap.animate({
                    height: slideHeight
                }, "normal");
                //$victim.animate({
                //    'padding-top': 0,
                //}, "normal");
                //$time.animate({
                //    'padding-top': 0,
                //}, "normal");

                $(this).text("Show all");
                $wrap.children(".gradient").fadeIn();
            }
            return false;
        });
    }
});
</script>
