<?php
$debug=false;
$regions=array();

echo '<!DOCTYPE HTML>
<html lan="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="stylesheet" href="liveview.css" type="text/css" />
<script>
setTimeout(function(){
   window.location.reload(1);
}, 5000);
</script>
</head>
<body onload="CCPEVE.requestTrust(\'https://yoursite.com/liveview.php\')">
';

$doc = new DOMDocument();
$doc->loadHTMLFile("camps.html");

function getElementsByClass(&$parentNode, $tagName, $className) {
    $nodes=array();

    $childNodeList = $parentNode->getElementsByTagName($tagName);
    for ($i = 0; $i < $childNodeList->length; $i++) {
        $temp = $childNodeList->item($i);
        if (stripos($temp->getAttribute('class'), $className) !== false) {
            $nodes[]=$temp;
        }
    }

    return $nodes;
}

$jumps = [];
$jump_node=$doc->getElementById("jumps");
$jump_divs=getElementsByClass($jump_node, 'div', 'jump');
foreach ($jump_divs as &$div) {
    $jumps[$div->getAttribute('id')] = $div->nodeValue;
}

$browser = $_SERVER['HTTP_USER_AGENT'];
if (!function_exists('getallheaders')) {
        function getallheaders() {
            foreach($_SERVER as $key=>$value) {
                if (substr($key,0,14)=="REDIRECT_HTTP_") {
                    $key=str_replace("REDIRECT_HTTP_", "", $key);
                    $out[$key]=$value;
                }else if (substr($key,0,5)=="HTTP_") {
                    $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5)))));
                    $out[$key]=$value;
                }else{
                    $out[$key]=$value;
        }
            }
            return $out;
        }
} 
if(stristr($browser, 'EVE-IGB')|| $debug) {
        $ingame = true;
        if (getallheaders()['EVE_TRUSTED']=='no' || getallheaders()['EVE_TRUSTED']=='No') {
                $trusted = false;
                echo "This feature requires trust";
                exit();
        } else {
                $trusted = true;
                $pilotname = getallheaders()['EVE_CHARNAME'];
                $corpId = getallheaders()['EVE_CORPID'];
                $allyId = getallheaders()['EVE_ALLIANCEID'];
                $systemName = getallheaders()['EVE_SOLARSYSTEMNAME'];
		$systemID = getallheaders()['EVE_SOLARSYSTEMID'];
        }
}
else {
	echo "This feature requires the IGB and trust";
	exit();
}

if ($debug) {
    $systemName = 'Kedama';
    $systemID = 30004975; 
}

$servername = "localhost";
$username = "";
$password = "";
$dbname = "";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT jump.toSolarSystemID as destID, dest.solarSystemName as destName, dest.security as sec, dest.regionID as region, origin.solarSystemID as originID, origin.solarSystemName as originName FROM mapSolarSystems dest LEFT JOIN mapSolarSystemJumps jump ON dest.solarSystemID = jump.toSolarSystemID LEFT JOIN mapSolarSystems origin ON origin.solarSystemID = jump.fromSolarSystemID WHERE origin.solarSystemID = ".(string)(int)$systemID;
$result = $conn->query($sql);
echo "<div><table>";
if ($result->num_rows > 0) {
    echo "Current system: ".$systemName;
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if ((int)$row["originID"] > (int)$row["destID"]) {
            $jump = "j-".$row["originID"]."-".$row["destID"];
        } else {
            $jump = "j-".$row["destID"]."-".$row["originID"];
        }
        if (array_key_exists($jump, $jumps)) {
            echo "<tr><td class='adjacent-camp' title='".$jumps[$jump]."'>Danger</td>";
        }else{
            if (in_array($row["region"], $regions)) {
                echo "<tr><td class='adjacent-ok'>Save</td>";
            }else{
                echo "<tr><td class='adjacent-undefined'>Undefined</td>";
            }
        }
        echo "<td class='adjacent' id='".$jump."'>".$row["destName"]." ";
        $sec = round($row["sec"], 1);
        if ($sec < 0) {
            $sec = 0.0;
        }
        echo "<small>(<span class=s".str_replace(".", "", $sec).">".round($row["sec"], 1)."</span>)</small></td></tr>";
    }
} else {
    echo "0 results";
}
echo "</table></div></body></html>";
$conn->close();
?>
