#!/usr/bin/python

import urllib2
import json
import xml.etree.cElementTree as ET
from time import mktime
import re
import time
from datetime import datetime, timedelta
from yattag import Doc,indent
from operator import itemgetter
import MySQLdb as mdb
import httplib
from hashlib import md5
import os, sys, zlib, gzip, StringIO
from BeautifulSoup import BeautifulSoup
from math import sqrt

debug = False

dirname=os.path.dirname(os.path.realpath(sys.argv[0]))

regions= [] #regionids as string
shipIDs = {'Svipul':34562,'Thrasher':16242,'Loki':29990,'Gnosis':3756,'Condor':583,'Slasher':585,'Stiletto':11198,'Crow':11176}
artillery = [487,8905,8907,8909,8903,2905,488,9413,9415,9417,9411,2977,492,9209,9213,9207,2921,493,9453,9455,9457,9451,2969]
excludefaction = [] #int
excludeships = [0, 670, 33328]
deployables = [33474, 33520, 33522, 33475, 33700, 33702, 33589, 33477, 33583, 33581, 33478, 33479]
slidingtime = 1.5


def safecall(f, default='Unknown', exception=Exception):
    '''Returns modified f. When the modified f is called and throws an
    exception, the default value is returned'''
    try:
        return f
    except exception:
        return default

def calculate_cache_path(cache_location, url):
    """Checks if [cache_location]/[hash_of_url].headers and .body exist
    """
    thumb = md5(url).hexdigest()
    header = os.path.join(cache_location, thumb + ".headers")
    body = os.path.join(cache_location, thumb + ".body")
    return header, body

def check_cache_time(path, max_age):
    """Checks if a file has been created/modified in the [last max_age] seconds.
    False means the file is too old (or doesn't exist), True means it is
    up-to-date and valid"""
    if not os.path.isfile(path):
        return False
    cache_modified_time = os.stat(path).st_mtime
    time_now = time.time()
    if cache_modified_time < time_now - max_age:
        # Cache is old
        return False
    else:
        return True

def exists_in_cache(cache_location, url, max_age):
    """Returns if header AND body cache file exist (and are up-to-date)"""
    hpath, bpath = calculate_cache_path(cache_location, url)
    if os.path.exists(hpath) and os.path.exists(bpath):
        return(
            check_cache_time(hpath, max_age)
            and check_cache_time(bpath, max_age)
        )
    else:
        # File does not exist
        return False

def store_in_cache(cache_location, url, response):
    """Tries to store response in cache."""
    hpath, bpath = calculate_cache_path(cache_location, url)
    try:
        outf = open(hpath, "w")
        headers = str(response.info())
        outf.write(headers)
        outf.close()

        outf = open(bpath, "w")
        outf.write(response.read())
        outf.close()
    except IOError:
        return True
    else:
        return False

class CacheHandler(urllib2.BaseHandler):
    """Stores responses in a persistant on-disk cache.

    If a subsequent GET request is made for the same URL, the stored
    response is returned, saving time, resources and bandwidth
    """
    def __init__(self, cache_location, max_age = 21600):
        """The location of the cache directory"""
        self.max_age = max_age
        self.cache_location = cache_location
        if not os.path.exists(self.cache_location):
            os.mkdir(self.cache_location)

    def default_open(self, request):
        """Handles GET requests, if the response is cached it returns it
        """
        if request.get_method() is not "GET":
            return None # let the next handler try to handle the request

        if exists_in_cache(
            self.cache_location, request.get_full_url(), self.max_age
        ):
            return CachedResponse(
                self.cache_location,
                request.get_full_url(),
                set_cache_header = True
            )
        else:
            return None

    def http_response(self, request, response):
        """Gets a HTTP response, if it was a GET request and the status code
        starts with 2 (200 OK etc) it caches it and returns a CachedResponse
        """
        if (request.get_method() == "GET"
            and str(response.code).startswith("2")
        ):
            if 'x-local-cache' not in response.info():
                # Response is not cached
                set_cache_header = store_in_cache(
                    self.cache_location,
                    request.get_full_url(),
                    response
                )
            else:
                set_cache_header = True
            #end if x-cache in response

            return CachedResponse(
                self.cache_location,
                request.get_full_url(),
                set_cache_header = set_cache_header
            )
        else:
            return response

    def https_response(self, request, response):
        return self.http_response(request,response)

class CachedResponse(StringIO.StringIO):
    """An urllib2.response-like object for cached responses.

    To determine if a response is cached or coming directly from
    the network, check the x-local-cache header rather than the object type.
    """
    def __init__(self, cache_location, url, set_cache_header=True):
        self.cache_location = cache_location
        hpath, bpath = calculate_cache_path(cache_location, url)

        StringIO.StringIO.__init__(self, file(bpath).read())

        self.url     = url
        self.code    = 200
        self.msg     = "OK"
        headerbuf = file(hpath).read()
        if set_cache_header:
            headerbuf += "x-local-cache: %s\r\n" % (bpath)
        self.headers = httplib.HTTPMessage(StringIO.StringIO(headerbuf))

    def info(self):
        """Returns headers
        """
        return self.headers

    def geturl(self):
        """Returns original URL
        """
        return self.url

    def recache(self):
        new_request = urllib2.urlopen(self.url)
        set_cache_header = store_in_cache(
            self.cache_location,
            new_request.url,
            new_request
        )
        CachedResponse.__init__(self, self.cache_location, self.url, True)


def fetchgzipcache(url):
        req = urllib2.Request(url, headers={"Accept-Encoding":"gzip, deflate"})
        opener = urllib2.build_opener(CacheHandler("/tmp/"))
        res = opener.open(req)
        if res.code==200:
                if res.headers.getheader("Content-Encoding").find("gzip")!=-1:
                        # the urllib2 file-object dont support tell/seek, repacking in StringIO
                        fp = gzip.GzipFile(fileobj = StringIO.StringIO(res.read()))
                        data = fp.read()
                elif res.headers.getheader("Content-Encoding").find("deflate")!=-1:
                        data = zlib.decompress(res.read())
                else:
                        data = res.read()
        else:
                print "Error  while fetching ..." % res.msg
                sys.exit(-1)
        if debug:
                print "read %s bytes (compression: %s), decompressed to %d bytes" % (
                res.headers.getheader("Content-Length"),
                res.headers.getheader("Content-Encoding"),
                len(data))
                print res.headers
        return data


def fetchgzip(url):
	req = urllib2.Request(url, headers={"Accept-Encoding":"gzip, deflate"})
	res = urllib2.urlopen(req)
	if res.getcode()==200:
		if res.headers.getheader("Content-Encoding").find("gzip")!=-1:
			# the urllib2 file-object dont support tell/seek, repacking in StringIO
			fp = gzip.GzipFile(fileobj = StringIO.StringIO(res.read()))
			data = fp.read()
		elif res.headers.getheader("Content-Encoding").find("deflate")!=-1:
        		data = zlib.decompress(res.read())
    		else:
        		data = res.read()
	else:
    		print "Error  while fetching ..." % res.msg
    		sys.exit(-1)
	if debug:
		print "read %s bytes (compression: %s), decompressed to %d bytes" % (
    		res.headers.getheader("Content-Length"),
    		res.headers.getheader("Content-Encoding"),
    		len(data))
	return data

evetime = ET.ElementTree(file=urllib2.urlopen('https://api.eveonline.com/server/ServerStatus.xml.aspx'))
timeroot = evetime.getroot()
for elem in timeroot.iter('currentTime'):
	timestamp = time.strptime(elem.text, '%Y-%m-%d %H:%M:%S')
starttime = (datetime.fromtimestamp(mktime(timestamp)) - timedelta(hours=slidingtime))
start = starttime.strftime('%Y%m%d%H%M')
zkburl = 'https://zkillboard.com/api/kills/regionID/'+','.join(regions)+'/startTime/'+start+'/shipID/'+','.join(str(x) for x in shipIDs.values())+'/'
if debug:
    print 'zkburl: '+zkburl
data = json.loads(fetchgzip(zkburl))
if (len(data)) >= 200:
	last = (data[199]["killID"])
	loop = True
	while loop:
		zkburl = 'https://zkillboard.com/api/kills/regionID/'+','.join(regions)+'/startTime/'+start+'/afterKillID/'+str(last)+'/shipID/'+','.join(str(x) for x in shipIDs.values())+'/'
		loopdata = json.loads(fetchgzip(zkburl))
		if (len(loopdata)) < 200:
			loop = False
		else:
			last = (loopdata[199]["killID"])
		data.extend(loopdata)

if debug:
	print 'dataentries: '+str(len(data))

camps = []
jumps = {}

con = mdb.connect('localhost', 'dbuser', 'dbpass', 'dbname')
cur = con.cursor()

for elem in sorted(data, key=itemgetter('killID'), reverse=True):
	acamp={}
	exclude = False
	camp = False
	ships = []
	shipnames = []
	parties= []
	if elem["victim"]["shipTypeID"] in deployables:
		exclude = True
	elif elem["victim"]["characterID"] == 0:
		exclude = True
	for attacker in elem["attackers"]:
		if (attacker["factionID"] in excludefaction) and not (elem["victim"]["factionID"] in excludefaction):
			exclude = True
		if attacker["characterID"] == 0:
                        exclude = True
		if attacker["allianceID"] != 0:
			party = attacker["characterName"]+' - '+attacker["allianceName"] 
		else:
			party = attacker["characterName"]+' - '+attacker["corporationName"]
		parties.append(party)
		if attacker["shipTypeID"] in [shipIDs["Svipul"],shipIDs["Thrasher"],shipIDs["Loki"]]:
			if attacker["weaponTypeID"] in [shipIDs["Svipul"],shipIDs["Thrasher"],shipIDs["Loki"]] or attacker["weaponTypeID"] in artillery:
				camp = True
		if attacker["shipTypeID"] in [shipIDs["Condor"],shipIDs["Slasher"],shipIDs["Stiletto"],shipIDs["Crow"]] and len(elem["attackers"]) >= 2:
                        camp = True
                if attacker["shipTypeID"] in [shipIDs["Gnosis"]]:
			camp = True
		if attacker["shipTypeID"] in excludeships:
			exclude = True
		ships.append(attacker["shipTypeID"])
                cur = con.cursor(mdb.cursors.Cursor)
		cur.execute("SELECT `TypeName` FROM `invTypes` WHERE `typeID` = "+str(attacker["shipTypeID"]))
		try:
			shipnames.append(cur.fetchone()[0])
		except:
			shipnames.append('Unknown ship')

	if debug:
		print('Kill :'+str(elem["killID"])+' camp: '+str(camp)+' exclude: '+str(exclude))

	if (not exclude) and camp:
		acamp["killID"]=elem["killID"]
		acamp["time"]=elem["killTime"]
		acamp["victim"]=elem["victim"]["characterID"]
		if elem["victim"]["allianceID"] != 0:
			acamp["victimname"]=elem["victim"]["characterName"]+' - '+elem["victim"]["allianceName"]
                else:
                        acamp["victimname"]=elem["victim"]["characterName"]+' - '+elem["victim"]["corporationName"]
		acamp["victimship"]=elem["victim"]["shipTypeID"]
                cur = con.cursor(mdb.cursors.Cursor)
		cur.execute("SELECT `TypeName` FROM `invTypes` WHERE `typeID` = "+str(elem["victim"]["shipTypeID"]))
		try:
			acamp["victimshipname"]=(cur.fetchone()[0])
		except:
			acamp["victimshipname"]='Unknown ship'
		killtime = time.strptime(acamp["time"], '%Y-%m-%d %H:%M:%S')
		delta = datetime.fromtimestamp(mktime(timestamp)) - datetime.fromtimestamp(mktime(killtime))
		minago = delta.seconds / 60
		if minago < 5:
			acamp["ago"]='less than 5 minutes ago.'
		elif minago < 15:
			acamp["ago"]='less than 15 minutes ago.'
		elif minago < 30:
			acamp["ago"]='less than half an hour ago.'
		elif minago < 60:
			acamp["ago"]='less than an hour ago.'
		else:
			acamp["ago"]='more than an hour ago.'
		acamp["involved"]=len(elem["attackers"])
		acamp["ships"]=ships
		acamp["shipnames"]=shipnames
		acamp["parties"]=parties
		acamp["system"]=elem["solarSystemID"]
                cur = con.cursor(mdb.cursors.Cursor)
		cur.execute("SELECT `solarSystemName` FROM `mapSolarSystems` WHERE `solarSystemID` = "+str(elem["solarSystemID"]))
		acamp["systemname"]=cur.fetchone()[0]
                loc={}
                for key, value in (elem["position"]).iteritems():
                    loc[str(key)] = float(value)
                cur = con.cursor(mdb.cursors.DictCursor)
                cur.execute("SELECT md.*,ms.solarSystemName as gate, ms.solarSystemID as destID FROM mapDenormalize md LEFT JOIN mapJumps mj ON md.itemID=mj.stargateID LEFT JOIN mapDenormalize mdd on mj.destinationID=mdd.itemID LEFT JOIN mapSolarSystems ms on mdd.solarSystemID=ms.solarSystemID WHERE md.solarSystemID = "+str(elem["solarSystemID"]))
                celestials=[]
                for cel in cur.fetchall():
                    distance = (sqrt((loc['x']-cel['x'])**2+(loc['y']-cel['y'])**2+(loc['z']-cel['z'])**2))-cel['radius']
                    if distance < 0:
                        distance=0
                    celes = []
                    if (cel['gate']) is not None:
                        celes.append(cel['gate']+' Gate')
			if int(cel['solarSystemID']) > int(cel['destID']):
                            jump = 'j-'+str(cel['solarSystemID'])+'-'+str(cel['destID'])
                        else:
                            jump = 'j-'+str(cel['destID'])+'-'+str(cel['solarSystemID'])
                        celes.append(jump)
                    elif (cel['itemName']) is not None:
                        celes.append(cel['itemName'])
                    else:
                        celes.append('Unknown location')
                    celestial=[]
                    celestial.append(distance)
                    celestial.append(celes)
                    celestials.append(celestial)
                nearest=(sorted(celestials)[0])
                if len(nearest[1]) > 1:
                    tempjump = {}
                    tempjump['minago'] = int(minago)
                    tempjump['string'] = nearest[1][0]+' in '+acamp["systemname"]
                    if debug:
                        print tempjump
                    if jump not in jumps:
                        jumps[jump] = tempjump
                    elif tempjump['minago'] < jumps[jump]['minago']:
                        jumps[jump] = tempjump
                    if debug:
                         print jumps
                acamp["location"] = nearest[1][0]
                distance_raw=round(nearest[0], 0)
                if distance_raw > 14959787070:
                     acamp["distance"] = str(round(distance_raw/149597870700, 1))+' AU'
                elif distance_raw > 1000000000:
                     acamp["distance"] = str(int(round(distance_raw/1000000000, 0)))+'M km'
                elif distance_raw > 1000000:
                     acamp["distance"] = str(int(round(distance_raw/1000000, 0)))+'k km'
                elif distance_raw > 1000:
                     acamp["distance"] = str(round(distance_raw/1000, 1))+' km'
                else:
                     acamp["distance"] = str(int(distance_raw))+' m'
		camps.append(acamp)
if con:
	con.close()

systems=[]
if len(camps) > 0:
	for elem in camps:
		if not elem["systemname"] in (x[0] for x in systems):
			systems.append([elem["systemname"], elem["system"]])

zkbwarning = False
ztop = fetchgzip('https://zkillboard.com/ztop.txt').splitlines()
zkbremaining = 0 
zkbsuccesful = 0 
zkbfailed = 0
for line in ztop:
	if debug:
		print(line)
	if re.match('\s*([0-9]*)\s*Kills remaining', line) is not None:
		zkbremaining = int(re.match('\s*([0-9]*)\s*Kills remaining', line).group(1))
	elif re.match('\s*([0-9]*)\s*Successful CREST calls', line) is not None:
		zkbsuccesful = int(re.match('\s*([0-9]*)\s*Successful CREST calls', line).group(1))
        elif re.match('\s*([0-9]*)\s*Failed CREST calls', line) is not None:
		zkbfailed = int(re.match('\s*([0-9]*)\s*Failed CREST calls', line).group(1))
if debug:
	print zkbremaining
        print zkbsuccesful
        print zkbfailed
if (zkbremaining > 100) and (zkbfailed > zkbsuccesful):
	zkbwarning = True


	
doc, tag, text = Doc().tagtext()

doc.asis('<!DOCTYPE html>')
with tag('html'):
    with tag('body'):
        with tag('div', klass = 'warning', id = 'warning'):
		if zkbwarning:
			text('Warning: zKB is currently having trouble fetching from CREST, results may be unreliable!')
		else:
			text('')
	if len(systems) == 0:
		with tag('div', klass = 'no-results'):
			text("No suspicious kills during the last 2 hours")
	else:
		systems.sort()
		with tag('div', klass = 'results'):
			for system in systems:
				with tag('div', id="system-"+str(system[1]), klass='system'):
					with tag('h2'):
						text(system[0])
					with tag('table'):
						with tag('tr'):
							with tag('th', klass='victim'):
								text('Victim')
							with tag('th', klass='time'):
                                                		text('Time & Location')
							with tag('th', klass='involved'):
								text('Involved')
						for elem in camps:
							if elem["systemname"] == system[0]:
								with tag('tr'):
									with tag('td', klass='victim'):
										with tag('a', href='https://zkillboard.com/kill/'+str(elem["killID"])+'/', target='_blank'):
											doc.stag('img', klass='image', title=elem["victimshipname"], src='https://image.eveonline.com/InventoryType/'+str(elem["victimship"])+'_32.png')
										doc.stag('img', klass='image', title=elem["victimname"], src='https://image.eveonline.com/Character/'+str(elem["victim"])+'_32.jpg')
                                                                	with tag('td', klass='time'):
                                                                        	text(elem["ago"])
                                                                                doc.stag('br')
                                                                                with tag('span', klass='maxheight', title=elem["distance"]+' from '+elem["location"]):
                                                                                    with tag('span', klass='position'):
                                                                                        text(elem["location"])
                                                                                    with tag('span', klass='ellipsis'):
                                                                                        text(' ...')
                                                                                    doc.stag('span', klass='fill')
                                                                	with tag('td', klass='involved'):
										for i in range(len(elem["ships"])):
                                                                        		doc.stag('img', klass='image', title=elem["shipnames"][i]+' - '+safecall(elem["parties"][i]), src='https://image.eveonline.com/InventoryType/'+str(elem["ships"][i])+'_32.png')
        with tag('div', klass = 'jumps', id = 'jumps'):
                for ele in jumps:
                        with tag('div', klass = 'jump', id = ele):
                                text(str(jumps[ele]['minago'])+' minutes ago, '+jumps[ele]['string']+'.')
	with tag('div', klass = 'creationtime', id = 'creationtime'):
		text("Created "+time.strftime('%Y-%m-%d %H:%M:%S' ,timestamp)+" EVE time.")

result = indent(doc.getvalue())
if debug:
	outfile = file(dirname+'/debug.html', 'w')
else:
        outfile = file(dirname+'/camps.html', 'w')
outfile.write(result.encode('utf-8'))
outfile.close()
	
