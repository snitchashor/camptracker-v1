<?php
$debug=false;
?>

<script src="jquery-1.11.3.min.js"></script>
<script>
function initialise() {
<?php if(isset($systemID)) {print'    $("#iframe").contents().find("[id=rect'.$systemID.']").css({"stroke" : "white", "stroke-width" : "3"});
';}?>
    $( "#temp" ).load( "camps.html", function(data) {
      $("#time").html($("#creationtime"));
      $("#warn").html($("#warning"));
      $('div[class=system]').each(function(index,obj){
        if ($(obj).find('td').text().indexOf("5 minutes") >= 0) {
	  $("iframe").contents().find("[id=" + $(obj).attr('id').replace("system-","rect") + "]").attr({class:"camp1"});	
        } else if ($(obj).find('td').text().indexOf("less than half an hour") >= 0) {
          $("iframe").contents().find("[id=" + $(obj).attr('id').replace("system-","rect") + "]").attr({class:"camp2"});
        } else if ($(obj).find('td').text().indexOf("less than an hour") >= 0) {
          $("iframe").contents().find("[id=" + $(obj).attr('id').replace("system-","rect") + "]").attr({class:"camp3"});
        } else {
          $("iframe").contents().find("[id=" + $(obj).attr('id').replace("system-","rect") + "]").attr({class:"camp4"});
        };
        $("iframe").contents().find("[id=" + $(obj).attr('id').replace("system-","rect") + "]").css({"cursor":"pointer"});
        $("iframe").contents().find("[id=" + $(obj).attr('id').replace("system-","rect") + "]").on( "click", function(event) {
          $("#popupdiv").html($(obj));
          $("#popup").css( {position:"absolute", display:"block", top:event.pageY, left: event.pageX});
        });
      });
      $('div[class=jump]').each(function(index,obj){
        $("iframe").contents().find("[id=" + $(obj).attr('id') + "]").attr({class:"jump1"});
      });
    });
}
$( window ).load(function(){
    initialise();
});

function closepopup() {
	 $("#popup").css( {display:"none"});
}
</script>
<div class="creationtime" id="time"></div>
<div id="temp" style="display: none;">test</div>
<div id="popup" style="display: none;" z-index="99999">
  <div style="position: absolute; right: 3px; top: 3px; z-index: 999;"> 
    <img src="close-icon.png" width="20" height="20" alt="close" style="cursor: pointer;" onclick="closepopup();" />
  </div>
  <div id="popupdiv" style="display: block;">
  </div>
</div>
