<?php
$title = "Snitch's Gatecamp Map";
require_once('header.php');
require('crestmap.php');
require('update.php');
echo '<div class="warning" id="warn"></div>
';
echo '<div class="map" style="background-color:black; overflow:hidden; width:1024px">
';
echo '<iframe id="iframe" src="newmap.svg" type="image/svg+xml" width="1024" height="768"></iframe>
';
echo '</div>';
include('mapscripts2.php');
require_once('footer.php');
?>
