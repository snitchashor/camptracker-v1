#!/usr/bin/python

import urllib2
import json
import xml.etree.cElementTree as ET
from time import mktime
import re, string
import time
from datetime import datetime, timedelta
from yattag import Doc,indent
from operator import itemgetter
import MySQLdb as mdb
import httplib, requests, urllib
from hashlib import md5
import os, sys, zlib, gzip, StringIO, getopt, socket, platform
from BeautifulSoup import BeautifulSoup
from math import sqrt
import logging
import logging.handlers

debug = False

dirname=os.path.dirname(os.path.realpath(sys.argv[0]))

regions= [] #regionids as integers
shipIDs = {'Svipul':34562,'Thrasher':16242,'Loki':29990,'Gnosis':3756,'Condor':583,'Slasher':585,'Stiletto':11198,'Crow':11176}
artillery = [487,8905,8907,8909,8903,2905,488,9413,9415,9417,9411,2977,492,9209,9213,9207,2921,493,9453,9455,9457,9451,2969]
excludefaction = [] #int
excludeships = [0, 670, 33328]
deployables = [33474, 33520, 33522, 33475, 33700, 33702, 33589, 33477, 33583, 33581, 33478, 33479]
slidingtime = 1.5

script_pid = ""
scriptName = "update-redisq"
compressed_logging = False
script_dir_path = "%s/logs/" % os.path.dirname(os.path.realpath(__file__))
zkb_exception_limit = 2
zkb_timeout = 15
redisq_url = 'https://redisq.zkillboard.com/listen.php'
retry_sleep = 1
retry_limit = 2
sleep_timer = 1
default_timeout = 11

if not os.path.exists(script_dir_path):
	os.makedirs(script_dir_path)

db_host = 'localhost'
db_user = ''
db_pass = ''
db_name = ''

cache = {}

f = logging.Formatter(fmt='%(message)s '
    '(%(asctime)s)',
    datefmt="%Y-%m-%d %H:%M:%S")
handlers = [
    logging.handlers.RotatingFileHandler(os.path.join(script_dir_path, 'update-redisq.log'), encoding='utf8',
        maxBytes=1024*1024, backupCount=2),
    logging.StreamHandler()
]
root_logger = logging.getLogger()
root_logger.setLevel(logging.ERROR)
for h in handlers:
    h.setFormatter(f)
    h.setLevel(logging.ERROR)
    root_logger.addHandler(h)

def CacheDecorator(func):

    def wrap(*args, **kwargs):

        key = str(func)+str(args)+str(kwargs)

        # cache for 5 seconds
        global lastTime
        if key not in cache:
            cache[key] = func(*args, **kwargs)

        return cache[key]

    return wrap

def safecall(f, default='Unknown', exception=Exception):
    '''Returns modified f. When the modified f is called and throws an
    exception, the default value is returned'''
    try:
        return f
    except exception:
        return default

def calculate_cache_path(cache_location, url):
    """Checks if [cache_location]/[hash_of_url].headers and .body exist
    """
    thumb = md5(url).hexdigest()
    header = os.path.join(cache_location, thumb + ".headers")
    body = os.path.join(cache_location, thumb + ".body")
    return header, body

def check_cache_time(path, max_age):
    """Checks if a file has been created/modified in the [last max_age] seconds.
    False means the file is too old (or doesn't exist), True means it is
    up-to-date and valid"""
    if not os.path.isfile(path):
        return False
    cache_modified_time = os.stat(path).st_mtime
    time_now = time.time()
    if cache_modified_time < time_now - max_age:
        # Cache is old
        return False
    else:
        return True

def exists_in_cache(cache_location, url, max_age):
    """Returns if header AND body cache file exist (and are up-to-date)"""
    hpath, bpath = calculate_cache_path(cache_location, url)
    if os.path.exists(hpath) and os.path.exists(bpath):
        return(
            check_cache_time(hpath, max_age)
            and check_cache_time(bpath, max_age)
        )
    else:
        # File does not exist
        return False

def store_in_cache(cache_location, url, response):
    """Tries to store response in cache."""
    hpath, bpath = calculate_cache_path(cache_location, url)
    try:
        outf = open(hpath, "w")
        headers = str(response.info())
        outf.write(headers)
        outf.close()

        outf = open(bpath, "w")
        outf.write(response.read())
        outf.close()
    except IOError:
        return True
    else:
        return False

class CacheHandler(urllib2.BaseHandler):
    """Stores responses in a persistant on-disk cache.

    If a subsequent GET request is made for the same URL, the stored
    response is returned, saving time, resources and bandwidth
    """
    def __init__(self, cache_location, max_age = 21600):
        """The location of the cache directory"""
        self.max_age = max_age
        self.cache_location = cache_location
        if not os.path.exists(self.cache_location):
            os.mkdir(self.cache_location)

    def default_open(self, request):
        """Handles GET requests, if the response is cached it returns it
        """
        if request.get_method() is not "GET":
            return None # let the next handler try to handle the request

        if exists_in_cache(
            self.cache_location, request.get_full_url(), self.max_age
        ):
            return CachedResponse(
                self.cache_location,
                request.get_full_url(),
                set_cache_header = True
            )
        else:
            return None

    def http_response(self, request, response):
        """Gets a HTTP response, if it was a GET request and the status code
        starts with 2 (200 OK etc) it caches it and returns a CachedResponse
        """
        if (request.get_method() == "GET"
            and str(response.code).startswith("2")
        ):
            if 'x-local-cache' not in response.info():
                # Response is not cached
                set_cache_header = store_in_cache(
                    self.cache_location,
                    request.get_full_url(),
                    response
                )
            else:
                set_cache_header = True
            #end if x-cache in response

            return CachedResponse(
                self.cache_location,
                request.get_full_url(),
                set_cache_header = set_cache_header
            )
        else:
            return response

    def https_response(self, request, response):
        return self.http_response(request,response)

class CachedResponse(StringIO.StringIO):
    """An urllib2.response-like object for cached responses.

    To determine if a response is cached or coming directly from
    the network, check the x-local-cache header rather than the object type.
    """
    def __init__(self, cache_location, url, set_cache_header=True):
        self.cache_location = cache_location
        hpath, bpath = calculate_cache_path(cache_location, url)

        StringIO.StringIO.__init__(self, file(bpath).read())

        self.url     = url
        self.code    = 200
        self.msg     = "OK"
        headerbuf = file(hpath).read()
        if set_cache_header:
            headerbuf += "x-local-cache: %s\r\n" % (bpath)
        self.headers = httplib.HTTPMessage(StringIO.StringIO(headerbuf))

    def info(self):
        """Returns headers
        """
        return self.headers

    def geturl(self):
        """Returns original URL
        """
        return self.url

    def recache(self):
        new_request = urllib2.urlopen(self.url)
        set_cache_header = store_in_cache(
            self.cache_location,
            new_request.url,
            new_request
        )
        CachedResponse.__init__(self, self.cache_location, self.url, True)

class zkbException(Exception):
	def __init__ (self, exception, message):
		self.message = message
		self.source_error = exception
	def __str__ (self):
		return "%s: %s" % (self.message, self.source_error)
	def __nonzero__ (self):
		return True

def writelog(pid, message):
        logging.error(message)

def get_lock(process_name):
	#Stolen from: http://stackoverflow.com/a/7758075
	global lock_socket   # Without this our lock gets garbage collected
	lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        writelog(script_pid, "Started with PID "+script_pid)
	try:
		lock_socket.bind('\0' + process_name)
		writelog(script_pid, "PID-Lock acquired")
	except socket.error:
		writelog(script_pid, "PID already locked.  Quitting")
		sys.exit()

def fetch_data(pid, debug=debug):
	#if debug: print "\tfetch_data()"
	fetch_url = redisq_url
	POST_values = {
		'accept-encoding' : 'gzip, deflate',
		'user-agent'      : 'python-request',
		}
	last_error = False
	for tries in range (0,retry_limit):
		time.sleep(sleep_timer * tries)
		try:
			request = requests.post(
				fetch_url, 
				data=POST_values,
				timeout=(default_timeout, zkb_timeout)
				)			
		except requests.exceptions.ConnectionError as e:
			last_error = zkbException(e, 'requests.ConnectionError tries=%s' % tries)
			writelog( pid, last_error )
			continue
		except requests.exceptions.ConnectTimeout as e:	
			last_error =  zkbException(e, 'requests.ConnectionTimeout tries=%s' % tries)
			writelog( pid, last_error )
			continue
		except requests.exceptions.ReadTimeout as e:	
			last_error = zkbException(e, 'requests.ReadTimeout tries=%s' % tries)
			writelog( pid, last_error )
			continue
		
		if request.status_code == requests.codes.ok:
			try:
				request.json()
			except ValueError as e:
				last_error = zkbException(e, 'response not JSON tries=%s' % tries)
				writelog( pid, last_error )
				continue
			break	#if all OK, break out of error checking
		else:
			last_error = zkbException(request.status_code, 'bad status code tries=%s' % tries)
			writelog( pid, last_error )
			continue
	else:
		raise last_error	#let main handle final crash/retry logic 
	return request.json()

def test_killInfo (kill_obj, pid=script_pid, debug=debug):
	#if debug: print "save_killInfo()"
	kill_info = kill_obj['package']
	if kill_info == None:
		writelog(pid, "ERROR: empty response")
		raise zkbException(EOFError, "empty response")
		#return "empty response"
	try:	#check that the critical pieces of any kill are in tact
		killID		= int(kill_obj['package']['killID'])
		hash 			= kill_info['zkb']['hash']
		killTime	= kill_info['killmail']['killTime']
	except KeyError as e:
		writelog(pid, "ERROR: critical key check failed: %s" % e)
		raise zkbException(e, "ERROR: critical key check failed")
		#return e #let main handle final crash/retry logic 	
	except TypeError as e:
		writelog(pid, "ERROR: critical key check failed: %s" % e)
		raise zkbException(e, "ERROR: critical key check failed")
		#return e #let main handle final crash/retry logic 		
	if debug: print "%s @ %s" % (killID, killTime)
		
	try:
		killTime_datetime = datetime.strptime(killTime, "%Y.%m.%d %H:%M:%S") #2015.12.06 02:12:30
	except ValueError as e:
		writelog(pid, "ERROR: unable to convert `killTime`:%s %s" % (killTime, e))	
		raise zkbException(e, "ERROR: unable to convert `killTime`:%s" % killTime)
		#raise e #let main handle final crash/retry logic 
		#return e #let main handle final crash/retry logic 
	
	writelog(pid, "killID: %s PASS: critical key check" % killID)
	return '' #return empty string

def fetchgzipcache(url):
        req = urllib2.Request(url, headers={"Accept-Encoding":"gzip, deflate"})
        opener = urllib2.build_opener(CacheHandler("/tmp/"))
        res = opener.open(req)
        if res.code==200:
                if res.headers.getheader("Content-Encoding").find("gzip")!=-1:
                        # the urllib2 file-object dont support tell/seek, repacking in StringIO
                        fp = gzip.GzipFile(fileobj = StringIO.StringIO(res.read()))
                        data = fp.read()
                elif res.headers.getheader("Content-Encoding").find("deflate")!=-1:
                        data = zlib.decompress(res.read())
                else:
                        data = res.read()
        else:
                print "Error  while fetching ..." % res.msg
                sys.exit(-1)
        if debug:
                print "read %s bytes (compression: %s), decompressed to %d bytes" % (
                res.headers.getheader("Content-Length"),
                res.headers.getheader("Content-Encoding"),
                len(data))
                print res.headers
        return data


def fetchgzip(url):
	req = urllib2.Request(url, headers={"Accept-Encoding":"gzip, deflate"})
	res = urllib2.urlopen(req)
	if res.getcode()==200:
		if res.headers.getheader("Content-Encoding").find("gzip")!=-1:
			# the urllib2 file-object dont support tell/seek, repacking in StringIO
			fp = gzip.GzipFile(fileobj = StringIO.StringIO(res.read()))
			data = fp.read()
		elif res.headers.getheader("Content-Encoding").find("deflate")!=-1:
        		data = zlib.decompress(res.read())
    		else:
        		data = res.read()
	else:
    		print "Error  while fetching ..." % res.msg
    		sys.exit(-1)
	if debug:
		print "read %s bytes (compression: %s), decompressed to %d bytes" % (
    		res.headers.getheader("Content-Length"),
    		res.headers.getheader("Content-Encoding"),
    		len(data))
	return data

def get_eve_time():
	evetime = ET.ElementTree(file=urllib2.urlopen('https://api.eveonline.com/server/ServerStatus.xml.aspx'))
	timeroot = evetime.getroot()
	for elem in timeroot.iter('currentTime'):
		timestamp = time.strptime(elem.text, '%Y-%m-%d %H:%M:%S')
        return timestamp

@CacheDecorator
def get_regionid(solarsystemid, con):
        cur = con.cursor(mdb.cursors.Cursor)
        cur.execute("SELECT `regionID` FROM `mapSolarSystems` WHERE `solarSystemID` = "+str(solarsystemid))
        return int(cur.fetchone()[0])

def process_kill(elem, con):
	acamp={}
	exclude = False
	camp = False
	ships = []
	shipnames = []
	parties= []
        elem["regionID"] = get_regionid(elem["solarSystem"]["id"], con)
        if debug:
		print(elem["regionID"])
	if elem["regionID"] not in regions:
		return
	if elem["victim"]["shipType"]["id"] in deployables:
		exclude = True
                return
	try: 
		if elem["victim"]["character"]["id"] == 0:
			exclude = True
	except KeyError:
		exclude = True
		return
        try: 
		elem["victim"]["alliance"]["id"]
	except:
		elem["victim"]["alliance"] = {'id': None, 'name': None}
	try:
		elem["victim"]["faction"]["id"]
	except:
                elem["victim"]["faction"] = {'id': None, 'name': None}
	for attacker in elem["attackers"]:
                try:
			if (attacker["faction"]["id"] in excludefaction) and not (elem["victim"]["faction"]["id"] in excludefaction):
				exclude = True
		except:
			pass
		try:
			if attacker["character"]["id"] == 0:
                        	exclude = True
		except KeyError:
			exclude = True
			return
		try:
			if attacker["shipType"]["id"] in [shipIDs["Svipul"],shipIDs["Thrasher"],shipIDs["Loki"]]:
				if attacker["weaponType"]["id"] in [shipIDs["Svipul"],shipIDs["Thrasher"],shipIDs["Loki"]] or attacker["weaponType"]["id"] in artillery:
					camp = True
			if attacker["shipType"]["id"] in [shipIDs["Condor"],shipIDs["Slasher"],shipIDs["Stiletto"],shipIDs["Crow"]] and len(elem["attackers"]) >= 2:
        	                camp = True
              	  	if attacker["shipType"]["id"] in [shipIDs["Gnosis"]]:
				camp = True
			if attacker["shipType"]["id"] in excludeships:
				exclude = True
		except KeyError:
			pass
        writelog(script_pid, 'Kill: '+str(elem["killID"])+' camp: '+str(camp)+' exclude: '+str(exclude))
	if debug:
		print('Kill: '+str(elem["killID"])+' camp: '+str(camp)+' exclude: '+str(exclude))

	if (not exclude) and camp:
		cur = con.cursor()
		add_kill = ("INSERT INTO kills "
               			"(killID, killTime, characterName, characterID, shipTypeID, shipTypeName, corporationName, corporationID, allianceName, allianceID, position, attackers, solarSystemID, solarSystemName, regionID) "
               			"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
		kill_data = ( 	elem["killID"],
				string.replace(elem["killTime"], ".", "-"),
				elem["victim"]["character"]["name"],
				elem["victim"]["character"]["id"],
				elem["victim"]["shipType"]["id"],
				elem["victim"]["shipType"]["name"],
				elem["victim"]["corporation"]["name"],
				elem["victim"]["corporation"]["id"],
                                elem["victim"]["alliance"]["name"],
                                elem["victim"]["alliance"]["id"],
                                str(elem["victim"]["position"]),
                                str(elem["attackers"]),
                                elem["solarSystem"]["id"],
                                elem["solarSystem"]["name"],
                                elem["regionID"],)

		cur.execute(add_kill, kill_data)
		con.commit()
                cur.close()

@CacheDecorator
def get_celestials(solarsystemid, con):
	cur = con.cursor(mdb.cursors.DictCursor)
        cur.execute("SELECT md.*,ms.solarSystemName as gate, ms.solarSystemID as destID FROM mapDenormalize md LEFT JOIN mapJumps mj ON md.itemID=mj.stargateID LEFT JOIN mapDenormalize mdd on mj.destinationID=mdd.itemID LEFT JOIN mapSolarSystems ms on mdd.solarSystemID=ms.solarSystemID WHERE md.solarSystemID = "+str(solarsystemid))
	return cur.fetchall()

def build_camp_list(timestamp, con):
	camps = []
	jumps = {}
        start = datetime.fromtimestamp(mktime(timestamp))-timedelta(minutes=slidingtime*60)
        cur = con.cursor(mdb.cursors.DictCursor)
        cur.execute(("SELECT * FROM `kills` WHERE `killTime` >= %s ORDER BY killID DESC"), (start))
        elems = cur.fetchall()
        cur.close()
        for elem in elems:
                acamp = {}
		acamp["killID"]=elem["killID"]
		acamp["time"]=elem["killTime"]
		acamp["victim"]=elem["characterID"]
		if elem["allianceID"] is not None:
			acamp["victimname"]=elem["characterName"]+' - '+elem["allianceName"]
                else:
                        acamp["victimname"]=elem["characterName"]+' - '+elem["corporationName"]
		acamp["victimship"]=elem["shipTypeID"]
                acamp["victimshipname"]=elem["shipTypeName"]
                #killtime = time.strptime(acamp["time"], '%Y-%m-%d %H:%M:%S') 
		delta = datetime.fromtimestamp(mktime(timestamp)) - acamp["time"]
		minago = delta.seconds / 60
		if minago < 5:
			acamp["ago"]='less than 5 minutes ago.'
		elif minago < 15:
			acamp["ago"]='less than 15 minutes ago.'
		elif minago < 30:
			acamp["ago"]='less than half an hour ago.'
		elif minago < 60:
			acamp["ago"]='less than an hour ago.'
		else:
			acamp["ago"]='more than an hour ago.'
		acamp["involved"]=len(elem["attackers"])
                acamp["parties"] = []
                for attacker in eval(elem["attackers"]):
			party = {}
			try:
				party["shipTypeID"] = (attacker["shipType"]["id"])
                        	party["shipTypeName"] = (attacker["shipType"]["name"])
                        except:
                                party["shipTypeID"] = 0
                                party["shipTypeName"] = 'Unknown ship'
                                writelog(script_pid, "ERROR parsing kill "+str(elem["killID"])+" attacker : "+str(attacker)+" setting unknownn ship.")
                                if debug:
                                        print("ERROR parsing kill "+str(elem["killID"])+" attacker : "+str(attacker)+" setting unknownn ship.")
                        try:
				party["pilot"] = attacker["character"]["name"] +" - "+ attacker["alliance"]["name"]
                        except:
				party["pilot"] = attacker["character"]["name"] +" - "+ attacker["corporation"]["name"]
                        acamp["parties"].append(party)
		acamp["system"]=elem["solarSystemID"]
		acamp["systemname"]=elem["solarSystemName"]
                loc={}
                for key, value in eval(elem["position"]).iteritems():
                    loc[str(key)] = float(value)
                celestials=[]
                for cel in get_celestials(elem["solarSystemID"], con):
                    distance = (sqrt((loc['x']-cel['x'])**2+(loc['y']-cel['y'])**2+(loc['z']-cel['z'])**2))-cel['radius']
                    if distance < 0:
                        distance=0
                    celes = []
                    if (cel['gate']) is not None:
                        celes.append(cel['gate']+' Gate')
			if int(cel['solarSystemID']) > int(cel['destID']):
                            jump = 'j-'+str(cel['solarSystemID'])+'-'+str(cel['destID'])
                        else:
                            jump = 'j-'+str(cel['destID'])+'-'+str(cel['solarSystemID'])
                        celes.append(jump)
                    elif (cel['itemName']) is not None:
                        celes.append(cel['itemName'])
                    else:
                        celes.append('Unknown location')
                    celestial=[]
                    celestial.append(distance)
                    celestial.append(celes)
                    celestials.append(celestial)
                nearest=(sorted(celestials)[0])
                if len(nearest[1]) > 1:
                    tempjump = {}
                    tempjump['minago'] = int(minago)
                    tempjump['string'] = nearest[1][0]+' in '+elem["solarSystemName"]
                    if debug:
                        print tempjump
                    if jump not in jumps:
                        jumps[jump] = tempjump
                    elif tempjump['minago'] < jumps[jump]['minago']:
                        jumps[jump] = tempjump
                    if debug:
                         print jumps
                acamp["location"] = nearest[1][0]
                distance_raw=round(nearest[0], 0)
                if distance_raw > 14959787070:
                     acamp["distance"] = str(round(distance_raw/149597870700, 1))+' AU'
                elif distance_raw > 1000000000:
                     acamp["distance"] = str(int(round(distance_raw/1000000000, 0)))+'M km'
                elif distance_raw > 1000000:
                     acamp["distance"] = str(int(round(distance_raw/1000000, 0)))+'k km'
                elif distance_raw > 1000:
                     acamp["distance"] = str(round(distance_raw/1000, 1))+' km'
                else:
                     acamp["distance"] = str(int(distance_raw))+' m'
		camps.append(acamp)

	systems=[]
	if len(camps) > 0:
		for elem in camps:
			if not elem["systemname"] in (x[0] for x in systems):
				systems.append([elem["systemname"], elem["system"]])
	return (camps, systems, jumps)

def zkb_warning():
	zkbwarning = False
	ztop = fetchgzip('https://zkillboard.com/ztop.txt').splitlines()
	zkbremaining = 0 
	zkbsuccesful = 0 
	zkbfailed = 0
	for line in ztop:
		if re.match('\s*([0-9]*)\s*Kills remaining', line) is not None:
			zkbremaining = int(re.match('\s*([0-9]*)\s*Kills remaining', line).group(1))
		elif re.match('\s*([0-9]*)\s*Successful CREST calls', line) is not None:
			zkbsuccesful = int(re.match('\s*([0-9]*)\s*Successful CREST calls', line).group(1))
	        elif re.match('\s*([0-9]*)\s*Failed CREST calls', line) is not None:
			zkbfailed = int(re.match('\s*([0-9]*)\s*Failed CREST calls', line).group(1))
	if debug:
		print zkbremaining
	        print zkbsuccesful
	        print zkbfailed
	if (zkbremaining > 100) and (zkbfailed > zkbsuccesful):
		zkbwarning = True
		writelog(script_pid, "WARNING: zKB has trouble.")
        else:
		writelog(script_pid, "zKB status ok.")
	return zkbwarning

def build_html(camps, systems, jumps, timestamp, zkbwarning):
	
	doc, tag, text = Doc().tagtext()

	doc.asis('<!DOCTYPE html>')
	with tag('html'):
	    with tag('body'):
	        with tag('div', klass = 'warning', id = 'warning'):
			if zkbwarning:
				text('Warning: zKB is currently having trouble fetching from CREST, results may be unreliable!')
			else:
				text('')
		if len(systems) == 0:
			with tag('div', klass = 'no-results'):
				text("No suspicious kills during the last 2 hours")
		else:
			systems.sort()
			with tag('div', klass = 'results'):
				for system in systems:
					with tag('div', id="system-"+str(system[1]), klass='system'):
						with tag('h2'):
							text(system[0])
						with tag('table'):
							with tag('tr'):
								with tag('th', klass='victim'):
									text('Victim')
								with tag('th', klass='time'):
	                                                		text('Time & Location')
								with tag('th', klass='involved'):
									text('Involved')
							for elem in camps:
								if elem["systemname"] == system[0]:
									with tag('tr'):
										with tag('td', klass='victim'):
											with tag('a', href='https://zkillboard.com/kill/'+str(elem["killID"])+'/', target='_blank'):
												doc.stag('img', klass='image', title=elem["victimshipname"], src='https://image.eveonline.com/InventoryType/'+str(elem["victimship"])+'_32.png')
											doc.stag('img', klass='image', title=elem["victimname"], src='https://image.eveonline.com/Character/'+str(elem["victim"])+'_32.jpg')
	                                                                	with tag('td', klass='time'):
	                                                                        	text(elem["ago"])
	                                                                                doc.stag('br')
	                                                                                with tag('span', klass='maxheight', title=elem["distance"]+' from '+elem["location"]):
	                                                                                    with tag('span', klass='position'):
	                                                                                        text(elem["location"])
	                                                                                    with tag('span', klass='ellipsis'):
	                                                                                        text(' ...')
	                                                                                    doc.stag('span', klass='fill')
	                                                                	with tag('td', klass='involved'):
											for party in elem["parties"]:
	                                                                        		doc.stag('img', klass='image', title=party["shipTypeName"]+' - '+party["pilot"], src='https://image.eveonline.com/InventoryType/'+str(party["shipTypeID"])+'_32.png')
	        with tag('div', klass = 'jumps', id = 'jumps'):
	                for ele in jumps:
	                        with tag('div', klass = 'jump', id = ele):
	                                text(str(jumps[ele]['minago'])+' minutes ago, '+jumps[ele]['string']+'.')
		with tag('div', klass = 'creationtime', id = 'creationtime'):
			text("Created "+time.strftime('%Y-%m-%d %H:%M:%S' ,timestamp)+" EVE time.")
	
	result = indent(doc.getvalue())
	if debug:
		outfile = file(dirname+'/debug.html', 'w')
	else:
	        outfile = file(dirname+'/camps.html', 'w')
	outfile.write(result.encode('utf-8'))
	outfile.close()
	
def main():
	global script_pid, debug
	script_pid = str(os.getpid())
	con = mdb.connect(db_host, db_user, db_pass, db_name)
	if platform.system() == "Windows":
		print "PID Locking not supported on windows"
		if debug: print "--DEBUG MODE-- Overriding PID lock"
		else: sys.exit(0)
	else:
		get_lock(scriptName)

	package_null = False
	kills_processed = 0
	fail_count = 0
        starttime = datetime.now()
	while (not package_null):
		caught_exception = ''
		try:
			kill_data = fetch_data(script_pid, debug)
		except Exception as e:
			caught_exception = e
		try:
			test_killInfo(kill_data, script_pid, debug)
			#caught_exception = "%s%s" % (caught_exception, custom_exception)
			#TODO: write custom Exception class for critical errors
		except Exception as e:
			caught_exception = e
		
		if caught_exception:	#check to see if parsing should end
			if debug:
				print caught_exception
			if kills_processed == 0:
				time.sleep(retry_sleep)
				if fail_count >= zkb_exception_limit:
					error_msg = '''EXCEPTION FOUND: no kills_processed, and fail_count exceeded.  Probable error.
	kills_processed = {kills_processed}
	fail_count = {fail_count}
	exception = {caught_exception}'''
					error_msg = error_msg.format(
						kills_processed = kills_processed,
						fail_count = fail_count,
						caught_exception = caught_exception
						)
					writelog(script_pid, error_msg)
                                        package_null = True
					
				writelog(script_pid, "EXCEPTION FOUND: but kills_processed = %s, retry case: %s" % (kills_processed,caught_exception))
				#kills_processed += 1
				fail_count += 1 
				continue
			elif kills_processed > 0:
				writelog(script_pid, "EXCEPTION FOUND: kills_processed = %s, sleep case %s" % (kills_processed,caught_exception))
				if fail_count >= zkb_exception_limit:
					package_null = True
				fail_count += 1
			else:
				writelog(script_pid, "EXCEPTION FOUND: invalid value for `kills_processed`=%s, exception=%s" % (kills_processed, caught_exception))
                                package_null = True
			if 'empty response' in str(caught_exception):
				package_null = True
				if debug:
					print('Empty package, stopping.')
                if (kill_data["package"] is not None) and (kill_data["package"] != "None"):
			process_kill(kill_data["package"]["killmail"], con)
		kills_processed += 1
		running = (datetime.now() - starttime).seconds
		if running > 60:
			if debug:
				print('Fetched for more than one Minute building html')
			starttime = datetime.now()
			timestamp = get_eve_time()
			camps, systems, jumps = build_camp_list(timestamp, con)
			zkbwarning = zkb_warning()
			build_html(camps, systems, jumps, timestamp, zkbwarning)
        timestamp = get_eve_time() 
	camps, systems, jumps = build_camp_list(timestamp, con)
        purge = datetime.fromtimestamp(mktime(timestamp))-timedelta(days=2)
        cur = con.cursor()
        cur.execute(("DELETE FROM `kills` WHERE `killTime` <= %s"), (purge))
        con.commit()
        cur.close()
	if con:
		con.close()
        zkbwarning = zkb_warning()
	build_html(camps, systems, jumps, timestamp, zkbwarning)

if __name__ == "__main__":
	main()
        writelog(script_pid, "Finished with PID "+script_pid)
