<?php
$page = $_REQUEST['p'];

switch ($page) {
    case 'map':
        include('campmap.php');
        break;
    case 'support':
        include('donations.php');
        break;
    case 'about':
        include('about.php');
        break;
    case 'liveview':
        include('liveview.php');
        break;
    case 'liveview2':
        include('liveview2.php');
        break;
    case 'map2':
        include('campmap2.php');
        break;
    case 'list':
    default:
       include('list.php');
}
?>
